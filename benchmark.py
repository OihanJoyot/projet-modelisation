from pynvml import *
import time
import matplotlib.pyplot as plt

nvmlInit()
h = nvmlDeviceGetHandleByIndex(0)
info = nvmlDeviceGetMemoryInfo(h)
A=[]


try:
    while True:
        nvmlInit()
        h = nvmlDeviceGetHandleByIndex(0)
        info = nvmlDeviceGetMemoryInfo(h)
        A.append(info.used)
        time.sleep(1)
except KeyboardInterrupt:
    pass

plt.plot(A)
plt.show()

