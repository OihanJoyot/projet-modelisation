import cupy as cp
import numpy as np
import matplotlib.pyplot as plt
from IMAGE import IMAGE
from skimage.draw import line_aa, line
from tqdm import tqdm
from main import *

def im_obs(n, T, alpha):
    """
    Déforme les images stocké dans T de dimension (n, 400, 400)
    par la convolution de la tache de Airy,
    le sous-échantillonage lié à la dalle du capteur et
    le bruit de Poisson.
    Renvoie une matrice de dimension (n, 20, 20)
    """

    _EX = cp.arange(-40, 40, 0.1, dtype=cp.float32)
    _EY = cp.arange(-40, 40, 0.1, dtype=cp.float32)
    EX, EY = cp.meshgrid(_EX, _EY)

    _h = h(EX, EY)

    _T = cp.zeros((n, 800, 800))
    _T[:, 200:600, 200:600] = T

    photo = cp.zeros((800, 800))
    photo[0:20, 0:20] = cp.ones((20, 20))/20/20
    
        
    fft  = cp.fft.rfft2
    ifft = lambda x : cp.fft.fftshift(cp.fft.irfft2(x), axes=(-2,-1))


    res = ifft(fft(_T)*fft(_h))[:, 200:600:20, 200:600:20]

    return cp.random.poisson(alpha*res)


def synth_mol(n, alpha=0.5, memory = 100):
    """
    Génere n diracs selon la densité de probabilité de IMAGE
    et les déforme selon le modèle optique.
    Renvoie un tenseur de dimension (n, 20, 20)
    """

    I = [memory]*(n//memory)+[n%memory]
    # Découpage
    l = [_synth_mol(i, alpha).get() for i in tqdm(I)]        
    # Réunification
    return np.concatenate(l, axis=0)


def _synth_mol(n, alpha=0.5):

    u = IMAGE

    _F = np.cumsum((u/np.sum(u)).reshape((-1)))
    F = cp.array(_F)
    
    T = cp.zeros((n, 400*400))
    T[cp.arange(n), cp.searchsorted(F, cp.random.random(n), side="right")]=1
    T = T.reshape((n, 400, 400))

    res = im_obs(n, T, alpha)

    return res


def synth_im(n=10000, f0=10, alpha=0.5, plot=False):
    """
    Tire aléatoirement n molécules selon la densité de IMAGE, les déforme selon alpha
    et estime leurs positions pour reconstituer l'image.
    f0 est le acteur de résolution pour alpha=1.
    ET est déteminé à partir du graphique de l'écart-type en fonction de alpha.
    """
    
    xy = estim_poisson(cp.array(synth_mol(n, alpha)), cp.full(n, alpha), memory=30, plot=True)

    f = f0*alpha**.5
    dim = min(int(f*20), 400)
    RES = np.zeros((dim, dim))

    x = (xy[1]+20)/40*dim
    y = (xy[0]+20)/40*dim
    _bool = (x<dim)*(x>0)*(y<dim)*(y>0)
    np.add.at(RES, (np.int_(x[_bool]), np.int_(y[_bool])), 1)
    
    if plot:
        plt.subplot(1,3,1)
        plt.imshow(IMAGE, cmap="gray")
        plt.subplot(1,3,2)  
        plt.imshow(im_obs(1, cp.array((IMAGE)), alpha)[0].get(), cmap="gray")
        plt.subplot(1,3,3)
        plt.imshow(RES, cmap="gray")
        plt.show()

    return RES

# plt.imshow(IMAGE, cmap="gray")
# plt.savefig("IMAGE.pdf")
# plt.imshow(im_obs(1, cp.array([IMAGE]), 0.5)[0].get(), cmap="gray")
# plt.savefig("IMAGE_obs.pdf")

# for i in [1,2,3,4]:
#     plt.imshow(synth_mol(1)[0], cmap="gray")
#     plt.savefig(f"{i}.pdf")


synth_im(100000, plot=True)