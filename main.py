import cupy as cp
import numpy as np
import matplotlib.pyplot as plt
from IMAGE import IMAGE
from skimage.draw import line_aa, line
from tqdm import tqdm
import os


t = cp.transpose
def tensorprod(x, y) : return cp.tensordot(x, y, 0)

# On va directement chercher les fonctions de bessel de l'API cuda
J1 = cp._core.create_ufunc('j1', ('f->f', 'd->d'), 'out0 = j1f(in0)')
J2 = cp._core.create_ufunc('j2', ('f->f', 'd->d'), 'out0 = jnf(2, in0)')
J3 = cp._core.create_ufunc('j3', ('f->f', 'd->d'), 'out0 = jnf(3, in0)')

def H(x):
    return (2*J1(x)/x)**2

def dH(x):
    return -8*J1(x)*J2(x)/x**2

def ddH(x):
    return 8*(
        J2(x)**2*x-J1(x)*(J2(x)-J3(x)*x)
    )/x**3

_E0=100; _N=5; _f=1

def h(x, y):
    _x = x+1e-10 ; _y = y+1e-10

    return _E0*H(cp.pi*(_x**2+_y**2)**.5/_N/_f)

def dh(x, y):
    _x = x+1e-10 ; _y = y+1e-10
    x2py2 = (_x**2+_y**2)
    x2py2_05 = x2py2**0.5
    x2py2_m05 = x2py2**-0.5

    f = cp.pi*x2py2_05/_N/_f
    df = cp.pi/_N/_f*cp.array(
        [x*x2py2_m05, y*x2py2_m05])
    return df*_E0*(dH)(f)

def ddh(x, y):
    _x = x+1e-10 ; _y = y+1e-10
    x2py2 = (_x**2+_y**2)
    x2py2_05 = x2py2**0.5
    x2py2_m05 = x2py2**-0.5
    x2py2_m15 = x2py2**-1.5

    f = cp.pi*x2py2_05/_N/_f
    df = cp.pi/_N/_f*cp.array(
        [_x*x2py2_m05, _y*x2py2_m05])
    ddf = cp.pi/_N/_f*cp.array([
        [x2py2_m05 - _x**2*x2py2_m15, -_y*_x*x2py2_m15],
        [-_y*_x*x2py2_m15, x2py2_m05 - _y**2*x2py2_m15]])
    dfdf = cp.array([[df[0]**2, df[0]*df[1]], [df[0]*df[1], df[1]**2]])
    return ddf*_E0*(dH)(f) + dfdf*_E0*(ddH)(f)





def gen_images_x0y0(alpha, x0=cp.pi, y0=cp.pi):
    """
    Pour chaque élément _alpha du tableau alpha : 
    Génère l'image d'une molécule positionnée en (x0, y0) et
    la bruite selon une loi de poisson de paramètre _alpha
    """
    _xrange = (-20, 20)
    _yrange = (-20, 20)
    _ZX = cp.arange(*_xrange, 2, dtype=cp.float32) #(20)
    _ZY = cp.arange(*_yrange, 2, dtype=cp.float32) #(20)
    ZX, ZY = cp.meshgrid(_ZX, _ZY)
    Y = cp.random.poisson(tensorprod(alpha, h(x0-ZX, y0-ZY))) #(.., 20, 20)
    return Y

def estim_poisson(Y, alpha=cp.array([1]), plot = False, memory = 50):
    """
    Estime la position (x0, y0) par la méthode du maximum de vraisemblance l

    Renvoie un tableau de dimension (2, *dim(alpha)) avec
        - res[0] de dimension dim(alpha) correspondant aux x des positions estimées
        - res[1] de dimension dim(alpha) correspondant aux y des positions estimées
    """

    s_alpha = alpha.shape
    alpha = alpha.reshape((-1)) #(n)

    s_Y = Y.shape
    Y = Y.reshape((-1, 20, 20)) #(n, 20, 20)
    
    # Découpage de alpha en des tableaux de taille maximale memory pour pas engorger la mémoire GPU
    l = [_estim_poisson(_Y, _alpha, plot and i==0).get() for i, (_alpha, _Y) in enumerate(
        tqdm(zip(
            cp.array_split(alpha, alpha.size//memory+1),
            cp.array_split(Y, alpha.size//memory+1, axis=0)), total=alpha.size//memory+1))]

    # Réunification
    return np.concatenate(l, axis=1).reshape((2, *s_alpha))



def _estim_poisson(Y, alpha=cp.array([1]), plot = False):

    _xrange = (-20, 20)
    _yrange = (-20, 20)
    _ZX = cp.arange(*_xrange, 2, dtype=cp.float32) #(20)
    _ZY = cp.arange(*_yrange, 2, dtype=cp.float32) #(20)
    ZX, ZY = cp.meshgrid(_ZX, _ZY)


    if plot:
      plt.imshow(Y[0].get(), extent = (*_xrange, *_yrange), origin='lower', cmap="gray")
      plt.scatter(cp.pi, cp.pi, marker='x', color="r")
      plt.show()

    # Déclarations de l'estimateur, d'une fonction rapide d'échantillonage et de ses dérivées
    def l(x, y): #(n, 3)
        _X = -ZX+tensorprod(x, cp.ones(ZX.shape)) #(n, 3, 20, 20)
        _Y = -ZY+tensorprod(y, cp.ones(ZX.shape)) #(n, 3, 20, 20)
        
        _h = t(h(_X, _Y), [1, 2, 3, 0])  #(n, 3, 20, 20) -> (3, 20, 20, n)
        _Y = t(Y, [1, 2, 0]) #(n, 20, 40) -> (20, 20, n)

        a_h = alpha*_h #(3, 20, 20, n)
        S =t(_Y*cp.log(a_h) - a_h, [3, 0, 1 ,2]) #(3, 20, 20, n) -> (n, 3, 20, 20)
        return cp.sum(S, axis=(-1,-2)) #(n, 3)

    def quickl(dt=0.01):
        _EX = cp.arange(-40, 40, dt, dtype=cp.float32) #(80/dt)
        _EY = cp.arange(-40, 40, dt, dtype=cp.float32) #(80/dt)
        EX, EY = cp.meshgrid(_EX, _EY) #(80/dt, 80/dt)

        a_h = tensorprod(alpha, h(EX, EY)) #(n, 80/dt, 80/dt)
        a_lnh = cp.log(a_h) #(n, 80/dt, 80/dt)
        _Y = cp.zeros(a_h.shape) #(n, 80/dt, 80/dt)

        _Y[:, _Y.shape[-2]//4:3*_Y.shape[-2]//4:int(2/dt), _Y.shape[-1]//4:3*_Y.shape[-1]//4:int(2/dt)] = Y
        # (n, 20, 20)
        
        fft  = cp.fft.rfft2
        ifft = lambda x : cp.fft.fftshift(cp.fft.irfft2(x), axes=(-1,-2))

        return (ifft((fft(_Y)*fft(a_lnh)))-a_h).real[:,
            _Y.shape[-2]//4:3*_Y.shape[-2]//4, _Y.shape[-1]//4:3*_Y.shape[-1]//4] #(n, 20/dt, 20/dt)
        

    def dl(x, y): #(n, 3)
        _X = -ZX+tensorprod(x, cp.ones(ZX.shape)) #(n, 3, 20, 20)
        _Y = -ZY+tensorprod(y, cp.ones(ZX.shape)) #(n, 3, 20, 20)

        _h = t(h(_X, _Y), [1, 2, 3, 0])  #(n, 3, 20, 40) -> (3, 20, 20, n)
        _dh = t(dh(_X, _Y), [0, 2, 3, 4, 1]) #(2, n, 3, 20, 20) -> (2, 3, 20, 20, n)
        _Y = t(Y, [1, 2, 0]) #(n, 20, 20) -> (20, 20, n)

        S = t(_Y*_dh/_h - alpha*_dh, [0, 4, 1, 2, 3]) #(2, 3, 20, 20, n) -> (2, n, 3, 20, 20)
        return cp.sum(S, axis=(-1,-2)) #(2, n, 3)

    def ddl(x, y): #(n, 3)
        _X = -ZX+tensorprod(x, cp.ones(ZX.shape)) #(n, 3, 20, 20)
        _Y = -ZY+tensorprod(y, cp.ones(ZX.shape)) #(n, 3, 20, 20)

        _h = t(h(_X, _Y), [1, 2, 3, 0])  #(n, 3, 20, 20) -> (3, 20, 20, n)
        _dh = t(dh(_X, _Y), [0, 2, 3, 4, 1]) #(2, n, 3, 20, 20) -> (2, 3, 20, 20, n)
        _ddh = t(ddh(_X, _Y), [0, 1, 3, 4, 5, 2])  #(2, 2, n, 3, 20, 20) -> #(2, 2, 3, 20, 20, n)
        _Y = t(Y, [1, 2, 0]) #(n, 20, 20) -> (20, 20, n)

        S = t(
            _Y*(_ddh*_h-cp.array(
            [[_dh[0]**2, _dh[0]*_dh[1]], [_dh[0]*_dh[1], _dh[1]**2]])
            )/_h**2-alpha*_ddh,
            [0, 1, 5, 2, 3, 4]) #(2, 2, 3, 20, 20, n) -> (2, 2, n, 3, 20, 20)
        return cp.sum(S, axis=(-1,-2)) #(2, 2, n, 3)
    
    # Calcul du maximum de vraisemblance

    return maximum(quickl, l, dl, ddl, .1, _xrange, _yrange, plot=plot)

def maximum(quickg, g, dg, ddg, h=.1, _xrange=(-20, 20), _yrange=(-20, 20), plot=False):
    """
    Trouve le max d'une fonction g
    quickg(h) est une fonction qui échantillone g pour un pas de h
    """
    X = cp.arange(*_xrange, h, dtype=cp.float32) #(20/h)
    Y = cp.arange(*_yrange, h, dtype=cp.float32) #(20/h)
    tX, tY = cp.meshgrid(X, Y) #(20/h, 20/h)

    F = quickg(h) #(n, 20/h, 20/h)
    n = F.shape[0]

    # Recherche discrète de maxima potentiels

    dFx = F[:, 1:, :] - F[:, :-1, :] #(n, 20/h-1, 20/h)
    Tx = dFx < 0 #(n, 20/h-1, 20/h)
    Ix = cp.zeros(F.shape, dtype=bool) #(n, 20/h, 20/h)
    Ix[:, 1:-1, :] = Tx[:, 1:, :]*(1-Tx[:, :-1, :]) #(n, 20/h-2, 20/h)

    dFy = F[:, :, 1:] - F[:, :, :-1] #(n, 20/h, 20/h-1)
    Ty = dFy < 0 #(n, 20/h, 20/h-1)
    Iy = cp.zeros(F.shape, dtype=bool) #(n, 20/h, 20/h)
    Iy[:, :, 1:-1] = Ty[:, :, 1:]*(1-Ty[:, :, :-1]) #(n, 20/h, 20/h-2)

    def Newton(x, y):
        xk, yk = x, y #(n, 3)
        for _ in range(10):
            G = dg(xk, yk).T #(2, n, 3) -> (3, n, 2)
            H = ddg(xk, yk).T #(2, 2, n, 3) -> (3, n, 2, 2)
            p = -cp.linalg.solve(H, G).T * ( #(3, n, 2) -> (2, n, 3)
                cp.linalg.det(H)>0).T * ( #(3, n) -> (n, 3)
                cp.trace(H, axis1=-1, axis2=-2)<0).T #(3, n) -> (n, 3)
            #(2, n, 3)
            xk += cp.where(cp.isnan(p[0]), 0, p[0]) #(n,3)
            yk += cp.where(cp.isnan(p[1]), 0, p[1]) #(n,3)
  
        return xk, yk

    # On ne sélectionne qu'un nombre limité de max
    nbr_max = 3
    _I = cp.zeros((n, nbr_max), dtype=cp.int32) #(n, 3)
    for i in range(nbr_max):
        Ii = cp.argmax((Ix*Iy*F).reshape(n, -1), axis=-1) #(n)
        Ix.reshape(-1)[Ii] = 0 ; Iy.reshape(-1)[Ii] = 0
        _I[:, i] = Ii
    
    # Amélioration de la précision avec la méthode de Newton
    RX, RY = Newton(tX.reshape(-1)[_I], tY.reshape(-1)[_I]) #(n, 3)
    # Détermination des maxima globaux
    imax = cp.argmax(g(RX, RY), axis=1) #(n)

    if plot:
        plt.imshow((F[0].get()), extent = (*_xrange, *_yrange), origin='lower')
        plt.scatter(tX.reshape(-1)[_I][0].get(), tY.reshape(-1)[_I][0].get(), marker='+', label = "max trouvé discretement")
        plt.scatter(RX[0, imax].get(), RY[0, imax].get(), marker='+', label = "max trouvé avec newton")
        plt.scatter(cp.pi, cp.pi, marker='x', label = "position de la molecule")
        plt.legend()
        plt.show()

    return cp.array((RX[cp.arange(RX.shape[0]), imax], RY[cp.arange(RY.shape[0]), imax]))







def cramer_ET(x0=cp.pi, y0=cp.pi, I = cp.arange(0.1, 5, 0.05), n=80): #(m)
    """
    Compare la variance de l'estimateur avec la borne de Cramer-Rao
    alpha \in I
    n correspond au nombre de tirages pour évaluer la variance
    """

    _ZX = cp.arange(-20, 20, 2, dtype=cp.float32)
    _ZY = cp.arange(-20, 20, 2, dtype=cp.float32)
    ZX, ZY = cp.meshgrid(_ZX, _ZY)
    _h = h(cp.pi-ZX, cp.pi-ZY) #(20, 20)
    _dh = dh(cp.pi-ZX, cp.pi-ZY) #(2 ,20, 20)
    _dh2 = cp.array([[_dh[0]**2, _dh[0]*_dh[1]], [_dh[0]*_dh[1], _dh[1]**2]]) #(2, 2, 20, 20)
    
    CR = 1/cp.sum(tensorprod(I, _dh2/_h), axis=(-1,-2))[:, 0, 0]**.5

    ALPHA = cp.tile(I, (n,1)) #(n, m)
    ET = np.var(estim_poisson(gen_images_x0y0(ALPHA, x0, y0), ALPHA, plot=True)[0], 0)**.5
    I, CR = I.get(), CR.get()


    # Affichage

    fig, axs = plt.subplots(2,1)
    fig.subplots_adjust(hspace=0.03)

    axs[0].plot(I, CR, color= "gray")
    axs[0].scatter(I, ET, marker = "x", color= "red")
    axs[1].loglog(I, CR, color= "gray")
    axs[1].scatter(I, ET, marker = "x", color= "red")
    a, b = np.polyfit(np.log(I), np.log(ET), 1)
    axs[0].plot(I, np.exp(a*np.log(I)+b), color= "red", linestyle = "--", linewidth = 0.7)
    axs[1].loglog(I, np.exp(a*np.log(I)+b), color= "red", linestyle = "--", linewidth = 0.7)
    
    axs[0].xaxis.tick_top()

    axs[0].grid(linewidth=0.4)
    axs[1].grid(which="both", linewidth=0.4)

    fig.legend(['√(Borne de Cramer-Rao)', f"σ empirique (n={n}) de l'Estimateur", "Regression"],
        loc = "lower right", mode="expand", borderaxespad = 0, ncol=3)

    plt.suptitle("Précision maximale et estimée de l'Estimateur selon α")
    plt.show()
    return CR, ET


def facteur_resolution(x, y, alpha):
    """ Renvoie la borne de cramer-rao en fonction de x et y"""
    _ZX = cp.arange(-20, 20, 2, dtype=cp.float32) #(20, 20)
    _ZY = cp.arange(-20, 20, 2, dtype=cp.float32) #(20, 20)
    ZX, ZY = cp.meshgrid(_ZX, _ZY) #(20, 20)

    _X = -ZX+tensorprod(x, cp.ones(ZX.shape)) #(..., 20, 20)
    _Y = -ZY+tensorprod(y, cp.ones(ZX.shape)) #(..., 20, 20)

    _h = h(_X, _Y) #(..., 20, 20)
    _dh = dh(_X, _Y) #(2, ..., 20, 20)
    _dh2 = _dh[0]**2 #(..., 20, 20)


    CR = cp.sum(alpha*(_dh2/_h), axis=(-1, -2))**.5 #(20,20)

    return CR

# _ZX = cp.arange(-20, 20, 2, dtype=cp.float32)
# _ZY = cp.arange(-20, 20, 2, dtype=cp.float32)
# ZX, ZY = cp.meshgrid(_ZX, _ZY)
# plt.imshow((facteur_resolution(ZX, ZY, 1)).get())
# plt.colorbar()
# plt.show()

# import time
# b=time.time()
# CR, V = cramer_ET(cp.arange(0.1, 100, 0.5), 80)
# print(time.time()-b)

# b=time.time()
# CR, V = cramer_ET(19, 19, cp.arange(0.1, 2, 0.03), 80)
# print(time.time()-b)

_ZX = cp.arange(-20, 20, 0.01, dtype=cp.float32)
_ZY = cp.arange(-20, 20, 0.01, dtype=cp.float32)
ZX, ZY = cp.meshgrid(_ZX, _ZY)
plt.imshow((h(ZX, ZY)).get(), extent=(-20, 20, -20, 20))
plt.colorbar()
plt.show()

